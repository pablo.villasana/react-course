import { FunctionComponent } from 'react'; // importing FunctionComponent

type GreeterProps = {
  title: string,
  message: string
}

export const Greeter: FunctionComponent<GreeterProps> = ({ title, message }) =>
<>
  <h2>{ title }</h2>
  <p>
    { message }
  </p>
</>