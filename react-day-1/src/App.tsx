import logo from './logo.svg';
import { Greeter } from './components/Greeter';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <Greeter title='Hello Manu!' message='Nice to see you here'  />
    </div>
  );
}

export default App;
