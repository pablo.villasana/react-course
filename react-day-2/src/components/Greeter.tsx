import { FunctionComponent, useEffect, useState } from 'react';

type GreeterProps = {
  title: string,
  message: string
}

export const Greeter: FunctionComponent<GreeterProps> = ({ title, message }) => {
  const [data, setData] = useState<GreeterProps>();
  const [isLoading, setIsLoading] = useState<Boolean>(false);

  useEffect(() => {
    function greetVisitor(): void {
      setIsLoading(true);
      setTimeout(() => {
        setData({ title, message })
        setIsLoading(false);
      }, 3000);
    }
    greetVisitor();
  }, []);

  return (
    <>
      {isLoading && <h2>Loading...</h2>}
      {data && <><h2>{data?.title}</h2><p>{data?.message}</p></>}
    </>
  )
}